<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" type="text/css" href="default.css">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>phpfortune</title>
</head>
<body>
<div class="row">
<div class="col-12">

<?php
$files = array_merge(glob("files/.[!.]*"), glob("files/*"));
$file = NULL;
$match = 0;

// check for random
if($_GET['file'] == "random" || empty($_GET) || $_GET['file'] == ''){
	$rng = rand(0,count($files)-1);
	$file = $files[$rng];
	$match = 1;
} else {
	// check for dir traversal attempts
	for($i = 0; $i < count($files); $i++){
			if(!strcmp( "files/" . $_GET['file'], $files[$i])){
				$file = "files/" . $_GET['file'];
				$match = 1;
		}
	}
}

// end if no match
if(!$match){
	die;
}


echo "<a href='" . $file . "'>" . $file . "</a><br>\n";

$arr = explode("\n%", file_get_contents($file));

// -2 because explode makes no fucking sense
$rng = (rand(0,count($arr)-2));
#echo nl2br(str_replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", str_replace(" ", "&nbsp;", htmlentities($arr[$rng]))));
echo "<pre class='fortune'>" . htmlentities($arr[$rng]) . "</pre>\n";

?>
</div>
</div>
</body>
</html>
