<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" type="text/css" href="default.css">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>phpfortune</title>
</head>
<body>
<div class="row">
<div class="col-6">
<h1> Fortunes </h1>
<p>The UNIX <b>fortune</b> program is a simple program that displays one random line of text from a file. These text files are <i>fortune files</i> because they contain <i>cookies</i>. A <i>cookie</i> is randomly chosen and displayed. The <b>fortune</b> program was designed to display a humorous, cynical remark or idea when a user logs in or logs out. The purpose was to difuse project tensions.  </p>

<p>In modern times, most <b>fortune </b> programs are actually BSD style, supporting multi-line fortunes delimited by <b>\n%\n</b>. Modern BSD style fortune programs use a random access file to increase the speed at which fortunes can be displayed. </p>

<p> I have collected many fortune files and have made them available over the web. Select any of the links to choose a category. The <b>random</b> link will randomly select a category and choose a fortune from that category display. Refreshing the page will load a new fortune. Clicking the link at the top of the fortune page will allow you to download that file for offline use. </p>

<p class="warning"><small>Be aware, the offensive fortune files are offensive. Please please please only click on them if you are willing to take offense. Additionally, some of the non-offensive files may contain offensive content. I have not audited them. Don't complain about it if your feelings get hurt, just stop clicking on it. </small></p>
</div>
<div class="col-6">
<h1> Available files </h1>
<?php
$files = array_merge(glob("files/.[!.]*"), glob("files/*"));
echo "<div class='file'><a href='fortune-html.php?file=random'>random</a>&nbsp;</div>\n";

foreach($files as $f){
	$fname = explode('/', $f);
	echo "<div class='file'><a href='fortune-html.php?file=" . $fname[1] . "'>" . $fname[1] . "</a>&nbsp;</div>\n";
}

?>
</div>

<div class="col-12">
<h1> Fortunes in your terminal </h1>
<p> Each fortune file is listed out here. To view one specific fortune file, copy it's url but replace <b>fortunes-html</b> with <b>fortunes-term</b>. Or, you can choose not to add the GET request for a random fortune file each time </p>
<pre class="cmdbox">
# GNU/Linux, OpenBSD
$ curl https://0x19.org/fortune/fortune-term.php?file=random

# FreeBSD
$ fetch -o - https://0x19.org/fortune/fortune-term.php?file=random

# Plan 9
% hget 'https://0x19.org/fortune/fortune-term.php?file=random'
</pre>  
</div>

<div class="col-12">
<h1> Using these fortune files locally </h1>
<p> Since these are BSD style fortunes, we need to make a quick lookup database. Download one of these fortune files (ascii-art as the example).
<pre class="cmdbox">
# GNU/Linux
$ dnf install fortune-mod
$ strfile ascii-art 
$ cp ascii-art.dat ascii-art /usr/share/games/fortune/
$ fortune ascii-art

# OpenBSD
$ strfile ascii-art 
$ cp ascii-art.dat ascii-art /usr/share/games/fortune/
$ fortune ascii-art

# FreeBSD
$ strfile ascii-art 
$ cp ascii-art.dat ascii-art /usr/local/share/games/fortune/
$ fortune ascii-art
</pre>

<p> Plan 9's fortune program does not support BSD style fortune files. So I wrote my one. Use <a href="https://gitlab.com/binrc/bsdfortune">bsdfortune</a> </p>

<pre class="cmdbox">
% bsdfortune /path/to/fortune/file
</pre>  
</div>


<div class="col-12">
<hr>
		<small> The backend powering this website is licensed under the BSD 2 clause. <a href="https://gitlab.com/binrc/phpfortune">Source code is available here.</a></small>
</div>



</div>
</body>
</html>
